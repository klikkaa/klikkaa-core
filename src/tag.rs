//! Legion Tag types

/// A tag that all objects that are controlled by scripts will have
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Scripted;

/// Indicates the main camera which should be used for rendering
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct MainCamera;
