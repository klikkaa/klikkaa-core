//! Backend types and logic for klikkaa!
#![deny(missing_docs)]
#![allow(clippy::type_complexity)]
// Used only when the empty backend is being used to compile the project, which should only be done by rls/rust-analyzer
#![cfg_attr(
    not(any(
        feature = "vulkan",
        feature = "dx11",
        feature = "dx12",
        feature = "metal",
        feature = "gl"
    )),
    allow(dead_code, unused_extern_crates, unused_imports)
)]

// Backend bindings, will be expanded when more backends are officially supported
#[cfg(feature = "vulkan")]
pub extern crate gfx_backend_vulkan as back;

// When no backend is selected a dummy backend is used, once again should really only be used with rls/rust-analyzer
#[cfg(not(any(
    feature = "vulkan",
    feature = "dx11",
    feature = "dx12",
    feature = "metal",
    feature = "gl"
)))]
pub extern crate gfx_backend_empty as back;

#[macro_use]
extern crate anyhow;

extern crate gfx_hal as hal;

extern crate mlua as lua;

pub mod component;
pub mod rendering;
pub mod resource;
pub mod scripting;
pub mod system;
pub mod tag;
