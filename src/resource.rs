//! `Legion` resource types

use crate::rendering::backend::Backend;
use crate::rendering::state::RendererState;

/// A `Legion` resource that contains the rendering state which is used by the rendering system.
#[derive(Debug)]
pub struct Renderer(pub RendererState<Backend>);
