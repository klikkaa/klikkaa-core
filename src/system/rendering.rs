use legion::prelude::*;

use crate::rendering::Renderable;

use crate::component::{
    MeshComponent, PipelineComponent, PositionComponent, RotationComponent, TextureComponent,
};

use crate::resource::Renderer;

/// Creates a Legion system for rendering
pub fn render<'a>(world: &mut World, resources: &mut Resources) {
    // Theoretical changed mesh update function

    let renderables = {
        let mut renderables = vec![];

        let renderables_query = <(
            Read<PositionComponent>,
            Read<RotationComponent>,
            Read<MeshComponent>,
            Read<TextureComponent>,
            Read<PipelineComponent>,
        )>::query();

        for (position, rotation, mesh, texture, pipeline) in renderables_query.iter_mut(&mut *world)
        {
            renderables.push(Renderable::new(
                position.0,
                rotation.0,
                pipeline.0.clone(),
                texture.0.clone(),
                mesh.0.clone(),
            ));
        }

        renderables
    };

    // Unwrap since failure to aquire the renderer indicates a larger issue
    let mut renderer = resources.get_mut::<Renderer>().unwrap();

    renderer.0.render(renderables);
}
