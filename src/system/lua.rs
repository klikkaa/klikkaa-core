use legion::prelude::*;

use std::cell::UnsafeCell;
use std::sync::{Arc, Mutex};

use crate::component::{
    LuaComponent, MeshComponent, NameComponent, PipelineComponent, PositionComponent,
    RotationComponent, TextureComponent,
};
use crate::tag::Scripted;

use crate::scripting::component::{RawScriptingData, WrappedScriptingData};

/// Creates a Legion system for rendering
///
/// The mass amount of queries are required to pull all the components into the `SubWorld` during system iteration.
/// This allows lua to access the components.
pub fn create_lua_system() -> std::boxed::Box<(dyn legion::systems::schedule::Schedulable + 'static)>
{
    SystemBuilder::new("LuaSystem")
        .with_query(Read::<LuaComponent>::query())
        .with_query(Read::<PositionComponent>::query().filter(tag::<Scripted>()))
        .with_query(Read::<RotationComponent>::query().filter(tag::<Scripted>()))
        .with_query(Read::<MeshComponent>::query().filter(tag::<Scripted>()))
        .with_query(Read::<TextureComponent>::query().filter(tag::<Scripted>()))
        .with_query(Read::<PipelineComponent>::query().filter(tag::<Scripted>()))
        .with_query(Read::<NameComponent>::query().filter(tag::<Scripted>()))
        .build(move |command_buffer, world, _, queries| {
            for (_entity, lua_component) in queries.0.iter_entities(&*world) {
                // Welcome to the
                //                                      ,--.
                //     ,---,       ,---,              ,--.'|  ,----..       ,---,.,-.----.
                //   .'  .' `\    '  .' \         ,--,:  : | /   /   \    ,'  .' |\    /  \
                // ,---.'     \  /  ;    '.    ,`--.'`|  ' :|   :     : ,---.'   |;   :    \
                // |   |  .`\  |:  :       \   |   :  :  | |.   |  ;. / |   |   .'|   | .\ :
                // :   : |  '  |:  |   /\   \  :   |   \ | :.   ; /--`  :   :  |-,.   : |: |
                // |   ' '  ;  :|  :  ' ;.   : |   : '  '; |;   | ;  __ :   |  ;/||   |  \ :
                // '   | ;  .  ||  |  ;/  \   \'   ' ;.    ;|   : |.' .'|   :   .'|   : .  /
                // |   | :  |  ''  :  | \  \ ,'|   | | \   |.   | '_.' :|   |  |-,;   | |  \
                // '   : | /  ; |  |  '  '--'  '   : |  ; .''   ; : \  |'   :  ;/||   | ;\  \
                // |   | '` ,/  |  :  :        |   | '`--'  '   | '/  .'|   |    \:   ' | \.'
                // ;   :  .'    |  | ,'        '   : |      |   :    /  |   :   .':   : :-'
                // |   ,.'      `--''          ;   |.'       \   \ .'   |   | ,'  |   |.'
                // '---'                       '---'          `---`     `----'    `---'
                //          ,----,   ,----..            ,--.
                //        .'   .`|  /   /   \         ,--.'|    ,---,.
                //     .'   .'   ; /   .     :    ,--,:  : |  ,'  .' |
                //   ,---, '    .'.   /   ;.  \,`--.'`|  ' :,---.'   |
                //   |   :     ./.   ;   /  ` ;|   :  :  | ||   |   .'
                //   ;   | .'  / ;   |  ; \ ; |:   |   \ | ::   :  |-,
                //   `---' /  ;  |   :  | ; | '|   : '  '; |:   |  ;/|
                //     /  ;  /   .   |  ' ' ' :'   ' ;.    ;|   :   .'
                //    ;  /  /--, '   ;  \; /  ||   | | \   ||   |  |-,
                //   /  /  / .`|  \   \  ',  / '   : |  ; .''   :  ;/|
                // ./__;       :   ;   :    /  |   | '`--'  |   |    \
                // |   :     .'     \   \ .'   '   : |      |   :   .'
                // ;   |  .'         `---`     ;   |.'      |   | ,'
                // `---'                       '---'        `----'
                // *precious dev time, wasted*
                //
                // If you know a better way of doing this, please please please please PR the better way.
                //
                // Touching this code is considered `unsafe` unless you are 110% sure you know what you're doing
                // because this code is not safe, and isn't very friendly. Yes, it DOES bite. It hurts.
                // I have tried my best to mitigate any possible unsafety, but the possibility does remain that there are problems
                // but only the rust gods can save us now.

                // This stores pointers to the types and structures lua scripts need to access.
                // It gets consumed by `scripting_data` and turned into a box.
                let raw_scripting_data = RawScriptingData {
                    command_buffer,
                    sub_world: world,
                };

                // Transmute the lifetime to `static so it can be put into the lua state.
                // Despite the static lifetime, the storage WILL be dropped, so there shouldn't
                // be any leaking issues caused by this.
                let raw_scripting_data = unsafe {
                    std::mem::transmute::<RawScriptingData<'_>, RawScriptingData<'static>>(
                        raw_scripting_data,
                    )
                };

                // Put the data into an `UnsafeCell` which will be given to the lua state.
                let unsafe_data = UnsafeCell::new(raw_scripting_data.clone());

                let (wrapped_scripting_data, storage_arc) = {
                    // Create the arc separately from the component storage. This allows us to later
                    // pull the box out of the mutex to prevent any horrific runtime bugs.
                    let arc = Arc::new(unsafe_data);

                    (WrappedScriptingData(Arc::clone(&arc)), arc)
                };

                // This isn't unsafe, praise be the rust gods.
                let lua_state = lua_component.0.lock().unwrap();

                lua_state
                    .scope(|scope| {
                        // Write the `WrappedComponentStorage` into the lua state.
                        lua_state.globals().set(
                            "__engine_data".to_owned(),
                            scope.create_userdata(wrapped_scripting_data)?,
                        )?;

                        // Run the script for the lua object, this is also not unsafe.
                        lua_state.load(&lua_component.1).exec()
                    })
                    .unwrap();

                // Prevent possible memory leaks
                drop(storage_arc);
            }
        })
}
