//! Defines functions for engine default stages
//!
//! Currently the stage order is as follows:
//! ```
//! PreUpdate
//! GUI
//! Scripting
//! PreRender
//! Render
//! ```
//!
//! The scripting stage is not handled using legion systems, and it
//! is expected that external code either handles it themselves, or
//! uses the provided method for doing so.
//!
//! Additionally, third party code can add additional systems to any
//! step, or build the stages manually if they need to remove systems
//! that the engine adds by default.

use legion::prelude::*;
use legion::systems::schedule::Builder;

/// Creates a schedule builder with engine-implemented systems that need to run before the main update.
///
/// `PreUpdate` systems are systems that have data that needs to be updated before
/// the rest of the update can run. An example of this might be a system that handles
/// animation. Since the GUI and Scripting stages could depend on the data from this
/// system it is important that it is run before them both. Thus, we run it before the
/// update.
pub fn default_pre_update() -> Builder {
    // Currently there is no logic for pre_update so we return an empty
    // schedule builder

    Schedule::builder()
}

/// Creates a schedule builder with engine-implemented systems that handle GUI.
///
/// `GUI` systems are systems that handle the user interface. Anything that interacts
/// with the user interface may make sense in the system created by this, but ultimately
/// that is left up to the implementer.
pub fn default_gui() -> Builder {
    // Currently there is no logic for gui so we return an empty schedule
    // builder

    Schedule::builder()
}

// Scripting needs to be handled differently, and thus there is no `default_scripting`
// in this file. (Check the `scripting` subcrate)

/// Creates a schedule builder with engine-implemented systems that prepare data
/// and structures for rendering.
///
/// Since the renderer is supposed to be abstracted away from external code,
/// it is important to ensure that data such as vertex buffers, shaders, images,
/// and renderer state changes are processed before the engine renders.
///
/// An example of where this is required is for vertex buffers on meshes. The
/// mesh component only takes a buffer (`Vec`) of `Vertex`, hiding the fact that
/// the engine must put the buffer onto the GPU in order for it to be renderable.  
/// This stage handles that, ensuring that the data makes it onto the gpu without
/// any manual intervention from external code.
pub fn default_pre_render() -> Builder {
    // Currently there is no logic for gui so we return an empty schedule
    // builder

    Schedule::builder()
}
