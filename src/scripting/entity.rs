use anyhow::{Context, Error};

use lua::{Lua, UserData, UserDataMethods};

use legion::entity::Entity;
use legion::prelude::*;

use super::component::RawScriptingData;
use super::component::{LuaNameComponent, LuaPositionComponent};
use crate::component::{NameComponent, PositionComponent};
use crate::tag::Scripted;

/// A lua representation of an Entity
#[derive(Clone)]
pub struct LuaEntity(pub Entity);

impl LuaEntity {
    /// Creates a new entity and places it inside this structure
    pub fn from_new(state: &Lua) -> Result<Self, Error> {
        let command_buffer =
            unsafe { RawScriptingData::get_from_state(state).get_command_buffer() };

        // Queue an `add_component`
        let entity = command_buffer
            .insert((Scripted, ()), vec![()])
            .first()
            .unwrap();

        Ok(Self(*entity))
    }
}

impl UserData for LuaEntity {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("get_name_component", |state, this, _: ()| {
            let subworld = unsafe { RawScriptingData::get_from_state(state).get_sub_world() };

            let component = subworld.get_component::<NameComponent>(this.0);

            match component {
                Some(component_ref) => {
                    // Construct a LuaNameComponent
                    let component = LuaNameComponent(component_ref.0.clone(), Some(this.clone()));
                    Ok(Some(component))
                }
                None => Ok(None),
            }
        });
        methods.add_method("get_position_component", |state, this, _: ()| {
            let subworld = unsafe { RawScriptingData::get_from_state(state).get_sub_world() };

            let component = subworld.get_component::<PositionComponent>(this.0);

            match component {
                Some(component_ref) => {
                    // Construct a LuaNameComponent
                    let component = LuaPositionComponent(
                        (component_ref.0).x,
                        (component_ref.0).y,
                        (component_ref.0).z,
                        Some(this.clone()),
                    );
                    Ok(Some(component))
                }
                None => Ok(None),
            }
        });
    }
}
