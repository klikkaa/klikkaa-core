//! Implements types, functions, and data for scripting.
//!
//! Its recommended to prefix types that can be confused with non-lua
//! types with `Lua` to prevent confusion.

pub mod component;
mod entity;

pub use self::entity::LuaEntity;

use lua::Lua;

use anyhow::Context;

use self::component::{LuaNameComponent, LuaPositionComponent};

/// Inits a lua state used for scripting
pub fn init_scripting_state(state: &Lua) -> Result<(), anyhow::Error> {
    let globals = state.globals();

    globals.set(
        "Name",
        state
            .create_function(|_, name: String| Ok(LuaNameComponent(name, None)))
            .context("Failed to create `LuaNameComponent` constructor")?,
    )?;

    globals.set(
        "Entity",
        state
            .create_function(|state, (): _| Ok(LuaEntity::from_new(state).unwrap()))
            .context("Failed to create `LuaEntity` constructor")?,
    )?;

    globals.set(
        "Position",
        state
            .create_function(|_, (x, y, z): (f32, f32, f32)| {
                Ok(LuaPositionComponent(x, y, z, None))
            })
            .context("Failed to create `Position` constructor")?,
    )?;

    Ok(())
}
