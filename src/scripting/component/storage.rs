//! These structs handle passing ecs data into a lua state

use legion::command::CommandBuffer;
use legion::subworld::SubWorld;

use lua::{Lua, UserData};

use anyhow::Context;

use std::cell::UnsafeCell;
use std::sync::{Arc, Mutex};

/// Holds data which is used by scripts (specifically userdata
/// functions) which is used to modify the world's state.
#[derive(Clone)]
pub struct RawScriptingData<'a> {
    /// The `Legion` `CommandBuffer` which can be used to make
    /// changes to the world
    pub command_buffer: *mut CommandBuffer,
    /// The `Legion` `SubWorld` which can be used to get data
    /// from components
    pub sub_world: *const SubWorld<'a>,
}

/// A wrapper for `RawScriptingData` which implements `UserData`
///
/// Wraps a pointer to a `RawScriptingData`, this is useful
/// because it ensures that Rust's access rules will not be
/// violated at runtime. (Also this fulfills UserData's `Clone`
/// requirement)
#[derive(Clone)]
pub struct WrappedScriptingData<'a>(pub Arc<UnsafeCell<RawScriptingData<'a>>>);

impl<'a> RawScriptingData<'a> {
    /// Tries to get a `RawScriptingData` from a lua state.
    ///
    /// # Safety
    /// This function requires that the lua state has a valid `WrappedScriptingData` within it, otherwise it will panic.
    ///
    /// # Panics
    /// If the `WrappedScriptingData` doesn't exist in the lua state.
    ///
    /// If the `RawScriptingData` within the `WrappedScriptingData` is invalid / missing.
    pub unsafe fn get_from_state(state: &Lua) -> &Self {
        // Extract the component storage from the lua_state.
        let wrapped_component_storage = state
            .globals()
            .get::<String, WrappedScriptingData>(String::from("__engine_data"))
            .unwrap();

        &*wrapped_component_storage.0.get()
    }

    /// Tries to get a `CommandBuffer` from the `RawScriptingData`
    ///
    /// # Safety
    /// Will panic if the `RawScriptingData` is invalid / missing.
    ///
    /// # Panics
    /// If the `RawScriptingData` is invalid / missing.
    pub unsafe fn get_command_buffer(&self) -> &mut CommandBuffer {
        self.command_buffer.as_mut().with_context(|| "Failed to get `CommandBuffer` from `RawScriptingData`. Likely memory safety violation. Please open an issue on the GitLab repo.").unwrap()
    }

    /// Tries to get a `Subworld` from the `RawScriptingData`
    ///
    /// # Safety
    /// Will panic if the `RawScriptingData` is invalid / missing.
    ///
    /// # Panics
    /// If the `RawScriptingData` is invalid / missing.
    pub unsafe fn get_sub_world(&self) -> &SubWorld {
        self.sub_world.as_ref().context("Failed to get `SubWorld` from `RawScriptingData`. Likely memory safety violation. Please open an issue on the GitLab repo.").unwrap()
    }
}

impl<'a> UserData for WrappedScriptingData<'a> {}
