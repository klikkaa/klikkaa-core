//! Lua representations of ECS components, which allows them to be
//! accessed and modified from inside scripts

mod name;
mod position;
mod storage;

pub use name::LuaNameComponent;
pub use position::LuaPositionComponent;
pub use storage::{RawScriptingData, WrappedScriptingData};
