use anyhow::Context;

use legion::command::CommandBuffer;
use legion::entity::Entity;

use lua::prelude::*;
use lua::{MetaMethod, UserData, UserDataMethods};

use super::RawScriptingData;
use crate::component::NameComponent;
use crate::scripting::LuaEntity;

/// Represents a Name component
///
/// First argument is the value, second is the entity which this component is attached to
#[derive(Clone)]
pub struct LuaNameComponent(pub String, pub Option<LuaEntity>);

impl LuaNameComponent {
    fn update_component_on(&self, command_buffer: &CommandBuffer, entity: &Entity) {
        // Create the component
        let component = NameComponent(self.0.clone());

        // Add an update to the command buffer
        command_buffer.add_component::<NameComponent>(*entity, component);
    }
}

impl UserData for LuaNameComponent {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method_mut("attach_to", |lua_state, this, entity: LuaEntity| {
            let command_buffer =
                unsafe { RawScriptingData::get_from_state(lua_state).get_command_buffer() };

            // Queue an `add_component`
            command_buffer.add_component(entity.0, NameComponent(this.0.clone()));

            // Set the owner of the LuaNameComponent
            this.1 = Some(entity);

            Ok(())
        });

        methods.add_method("has_owner", |_, this, _: ()| Ok(this.1.is_some()));

        methods.add_meta_method(MetaMethod::Index, |state, this, key: String| {
            match key.as_ref() {
                "name" => Ok(Some(
                    this.0
                        .clone()
                        .to_lua(state)
                        .with_context(|| {
                            "Failed to convert `Name` in `LuaNameComponent` into a lua value"
                        })
                        .unwrap(),
                )),
                "owner" => match &this.1 {
                    Some(owner) => Ok(Some(
                        owner
                            .clone()
                            .to_lua(state)
                            .with_context(|| {
                                "Failed to convert `Owner` in `LuaNameComponent` into a lua value"
                            })
                            .unwrap(),
                    )),
                    None => Ok(None),
                },
                _ => Ok(None),
            }
        });

        // TODO! Actually update component
        methods.add_meta_method_mut(
            MetaMethod::NewIndex,
            |state, this, (key, value): (String, String)| match key.as_ref() {
                "name" => {
                    this.0 = value;

                    // Update the component on the owner
                    if let Some(owner) = &this.1 {
                        this.update_component_on(
                            unsafe { RawScriptingData::get_from_state(state).get_command_buffer() },
                            &owner.0,
                        );
                    }
                    Ok(())
                }
                _ => Err(lua::Error::MismatchedRegistryKey),
            },
        );
    }
}
