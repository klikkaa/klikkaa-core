use anyhow::Context;
use lua::prelude::*;
use lua::{MetaMethod, UserData, UserDataMethods};

use legion::command::CommandBuffer;
use legion::entity::Entity;

use super::RawScriptingData;
use crate::component::PositionComponent;
use crate::rendering::Vector3;
use crate::scripting::LuaEntity;

/// Represents a Name component
///
/// First argument is the value, second is the entity which this component is attached to
#[derive(Clone)]
pub struct LuaPositionComponent(pub f32, pub f32, pub f32, pub Option<LuaEntity>);

impl LuaPositionComponent {
    fn update_component_on(&self, command_buffer: &CommandBuffer, entity: &Entity) {
        // Create the component
        let component = PositionComponent(Vector3::from(self.0, self.1, self.2));

        // Add an update to the command buffer
        command_buffer.add_component::<PositionComponent>(*entity, component);
    }
}

impl UserData for LuaPositionComponent {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method_mut("attach_to", |lua_state, this, entity: LuaEntity| {
            let command_buffer =
                unsafe { RawScriptingData::get_from_state(lua_state).get_command_buffer() };

            // Create an instance of `PositionComponent`
            let component = PositionComponent(Vector3 {
                x: this.0,
                y: this.1,
                z: this.2,
            });

            // Queue an `add_component`
            command_buffer.add_component(entity.0, component);

            // Set the owner of the LuaNameComponent
            this.3 = Some(entity);

            Ok(())
        });

        methods.add_method("has_owner", |_, this, _: ()| Ok(this.3.is_some()));

        methods.add_meta_method(MetaMethod::Index, |state, this, key: String| {
            match key.as_ref() {
                "x" => Ok(Some(
                    this.0
                        .clone()
                        .to_lua(state)
                        .with_context(|| {
                            "Failed to convert `LuaVector3` in `LuaPositionComponent` into a lua value"
                        })
                        .unwrap(),
                )),
                "y" => Ok(Some(
                    this.1
                        .clone()
                        .to_lua(state)
                        .with_context(|| {
                            "Failed to convert `LuaVector3` in `LuaPositionComponent` into a lua value"
                        })
                        .unwrap(),
                )),
                "z" => Ok(Some(
                    this.2
                        .clone()
                        .to_lua(state)
                        .with_context(|| {
                            "Failed to convert `LuaVector3` in `LuaPositionComponent` into a lua value"
                        })
                        .unwrap(),
                )),
                "owner" => match &this.3 {
                    Some(owner) => Ok(Some(
                        owner
                            .clone()
                            .to_lua(state)
                            .with_context(|| {
                                "Failed to convert `Owner` in `LuaNameComponent` into a lua value"
                            })
                            .unwrap(),
                    )),
                    None => Ok(None),
                },
                _ => Ok(None),
            }
        });

        methods.add_meta_method_mut(
            MetaMethod::NewIndex,
            |state, this, (key, value): (String, f32)| match key.as_ref() {
                "x" => {
                    this.0 = value;

                    if let Some(owner) = &this.3 {
                        this.update_component_on(
                            unsafe { RawScriptingData::get_from_state(state).get_command_buffer() },
                            &owner.0,
                        );
                    }

                    Ok(())
                }
                "y" => {
                    this.1 = value;

                    if let Some(owner) = &this.3 {
                        this.update_component_on(
                            unsafe { RawScriptingData::get_from_state(state).get_command_buffer() },
                            &owner.0,
                        );
                    }

                    Ok(())
                }
                "z" => {
                    this.2 = value;

                    if let Some(owner) = &this.3 {
                        this.update_component_on(
                            unsafe { RawScriptingData::get_from_state(state).get_command_buffer() },
                            &owner.0,
                        );
                    }

                    Ok(())
                }
                _ => Err(lua::Error::MismatchedRegistryKey),
            },
        );
    }
}
