//! Defines rendering behavior and types for klikka
#![deny(missing_docs)]

pub use back as backend;

pub mod descriptor;
mod mesh;
mod renderable;
pub mod shader;
pub mod state;
mod uniform;

pub use mesh::Mesh;
pub use renderable::Renderable;
pub use uniform::Uniform;

use hal::format::{Aspects, Rgba8Srgb};
use hal::image::SubresourceRange;
use hal::window::Extent2D;

/// Holds a width and a height of type `T`
pub struct Dimensions<T> {
    width: T,
    height: T,
}

/// The color format used by the renderer
pub type ColorFormat = Rgba8Srgb;

/// The color range used by the renderer
pub const COLOR_RANGE: SubresourceRange = SubresourceRange {
    aspects: Aspects::COLOR,
    levels: 0..1,
    layers: 0..1,
};

/// The default entry point symbol name for shader programs
pub const ENTRY_POINT: &str = "main";

/// The dimensions to use when creating swapchains
///
/// TODO: Is this needed?
pub const DEFAULT_DIMENSIONS: Extent2D = Extent2D {
    width: 1920,
    height: 1080,
};

/// The Vertex type
#[derive(Debug, Clone, Copy)]
pub struct Vertex {
    /// Position
    pub pos: [f32; 2],
    /// UV
    pub uv: [f32; 2],
}

/// A Vector with 3 values
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vector3 {
    /// X
    pub x: f32,
    /// Y
    pub y: f32,
    /// Z
    pub z: f32,
}

impl Vector3 {
    /// TODO
    pub fn from(x: f32, y: f32, z: f32) -> Self {
        Vector3 { x, y, z }
    }
}

use hal::Instance;
use std::mem::ManuallyDrop;

/// TODO
pub fn create_backend(
    wb: winit::window::WindowBuilder,
    event_loop: &winit::event_loop::EventLoop<()>,
) -> state::BackendState<back::Backend> {
    let window = wb.build(event_loop).unwrap();
    let instance =
        back::Instance::create("gfx-rs colour-uniform", 1).expect("Failed to create an instance!");
    let surface = unsafe {
        instance
            .create_surface(&window)
            .expect("Failed to create a surface!")
    };
    let mut adapters = instance.enumerate_adapters();
    state::BackendState {
        instance: Some(instance),
        adapter: state::AdapterState::new(&mut adapters),
        surface: ManuallyDrop::new(surface),
        window,
    }
}
