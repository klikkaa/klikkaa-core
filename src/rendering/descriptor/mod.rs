//! Handles storing, writing to, and creating descriptor sets
mod layout;
mod set;

pub use layout::DescSetLayout;
pub use set::{DescSet, DescSetWrite};
