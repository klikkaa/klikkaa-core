use hal::pso::{DescriptorPool, DescriptorSetLayoutBinding};
use hal::{device::Device, Backend};

use std::sync::{Arc, Mutex};

use super::DescSet;
use crate::rendering::state::DeviceState;

/// Stores a backend-specific descriptor set layout, as well as the device it is on
#[derive(Debug)]
pub struct DescSetLayout<B: Backend> {
    /// The backend-specific descriptor set layout
    pub layout: Option<B::DescriptorSetLayout>,
    /// The device which is being used
    pub device: Arc<Mutex<DeviceState<B>>>,
}

impl<B: Backend> DescSetLayout<B> {
    /// Create a new `DescSetLayout`
    ///
    /// # Safety
    /// The layout binding must be correct, but its likely it won't crash and burn if it isn't (until you try to render something)
    pub unsafe fn new(
        device_arc: &Arc<Mutex<DeviceState<B>>>,
        bindings: &[DescriptorSetLayoutBinding],
    ) -> Self {
        let device = device_arc.lock().unwrap();
        let descriptor_set_layout = device
            .device
            .create_descriptor_set_layout(bindings, &[])
            .ok();
        drop(device);
        Self {
            layout: descriptor_set_layout,
            device: Arc::clone(&device_arc),
        }
    }

    /// Creates a descriptor set in the given descriptor pool, consuming `self`
    ///
    /// # Safety
    /// `self` must contain a "correct" descriptor set layout for how it is going to be used. The pool must also have enough space for another descriptor set.
    pub unsafe fn create_desc_set(self, descriptor_pool: &mut B::DescriptorPool) -> DescSet<B> {
        // Allocate a descriptor set from the pool, this will panic if the pool doesn't have space for another set
        let descriptor_set = descriptor_pool
            .allocate_set(self.layout.as_ref().unwrap())
            .unwrap();

        DescSet {
            layout: self,
            set: Some(descriptor_set),
        }
    }
}
