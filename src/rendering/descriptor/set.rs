use hal::pso::{Descriptor, DescriptorArrayIndex, DescriptorBinding, DescriptorSetWrite};
use hal::{device::Device, Backend};

use super::DescSetLayout;

/// An allocated descriptor set
#[derive(Debug)]
pub struct DescSet<B: Backend> {
    /// The backend-specific descriptor set representation
    pub set: Option<B::DescriptorSet>,
    /// The layout of the descriptor set
    pub layout: DescSetLayout<B>,
}

/// Used to write into a `DescSet`
pub struct DescSetWrite<W> {
    /// The binding of the descriptor set
    pub binding: DescriptorBinding,
    /// TODO
    pub array_offset: DescriptorArrayIndex,
    /// TODO
    pub descriptors: W,
}

impl<B: Backend> DescSet<B> {
    /// Write data into the descriptor set
    ///
    /// # Safety
    /// There are no checks that the write is safe
    pub unsafe fn write_to_state<'a, 'b: 'a, W>(
        &'b mut self,
        write: Vec<DescSetWrite<W>>,
        device: &mut B::Device,
    ) where
        W: IntoIterator,
        W::Item: std::borrow::Borrow<Descriptor<'a, B>>,
    {
        let descriptor_set = self.set.as_ref().unwrap();

        let write: Vec<_> = write
            .into_iter()
            .map(|desc| DescriptorSetWrite {
                binding: desc.binding,
                array_offset: desc.array_offset,
                descriptors: desc.descriptors,
                set: descriptor_set,
            })
            .collect();

        device.write_descriptor_sets(write);
    }

    /// Returns the layout of the descriptor set
    pub fn get_layout(&self) -> &B::DescriptorSetLayout {
        self.layout.layout.as_ref().unwrap()
    }
}
