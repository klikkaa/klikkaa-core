use hal::adapter::MemoryType;
use hal::buffer::SubRange;
use hal::buffer::Usage;
use hal::pso::Descriptor;
use hal::Backend;

use std::sync::{Arc, Mutex};

use super::descriptor::{DescSet, DescSetWrite};
use super::state::{BufferState, DeviceState};

/// Wraps the functionality of a Uniform
#[derive(Debug)]
pub struct Uniform<B: Backend> {
    buffer: Option<BufferState<B>>,
    /// The descriptor set of this uniform
    pub descriptor: Option<DescSet<B>>,
}

impl<B: Backend> Uniform<B> {
    /// Creates a new Uniform
    ///
    /// # Safety
    /// There is no validation of the data being passed, not the descriptor set
    pub unsafe fn new<T>(
        device: Arc<Mutex<DeviceState<B>>>,
        memory_types: &[MemoryType],
        data: &[T],
        mut desc: DescSet<B>,
        binding: u32,
    ) -> Self
    where
        T: Copy,
    {
        let buffer = BufferState::new(&device, &data, Usage::UNIFORM, memory_types);

        let buffer = Some(buffer);

        desc.write_to_state(
            vec![DescSetWrite {
                binding,
                array_offset: 0,
                descriptors: Some(Descriptor::Buffer(
                    buffer.as_ref().unwrap().get_buffer(),
                    SubRange::WHOLE,
                )),
            }],
            &mut device.lock().unwrap().device,
        );

        Uniform {
            buffer,
            descriptor: Some(desc),
        }
    }

    /// Returns the layout of the descriptor set used by the Uniform
    pub fn get_layout(&self) -> &B::DescriptorSetLayout {
        self.descriptor.as_ref().unwrap().get_layout()
    }
}
