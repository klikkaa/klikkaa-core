use glam::Quat;

use hal::Backend;

use std::sync::{Arc, RwLock};

use crate::rendering::state::{ImageState, PipelineState};
use crate::rendering::{Mesh, Vector3};

/// An object which holds relevant components that the renderer can render
pub struct Renderable<B: Backend> {
    /// The position of the Renderable
    pub position: Vector3,
    /// The rotation of the Renderable
    pub rotation: Quat,
    /// The pipeline to use to render the Renderable
    pub pipeline: Arc<RwLock<PipelineState<B>>>,
    /// The texture to use to render the Renderable
    pub texture: Arc<RwLock<ImageState<B>>>,
    /// The mesh to use to render the Renderable
    pub mesh: Arc<RwLock<Mesh<B>>>,
}

impl<B: Backend> Renderable<B> {
    /// Creates a new `Renderable` object which can be submitted to the renderer
    #[inline]
    pub fn new(
        position: Vector3,
        rotation: Quat,
        pipeline: Arc<RwLock<PipelineState<B>>>,
        texture: Arc<RwLock<ImageState<B>>>,
        mesh: Arc<RwLock<Mesh<B>>>,
    ) -> Self {
        Self {
            position,
            rotation,
            pipeline,
            texture,
            mesh,
        }
    }
}
