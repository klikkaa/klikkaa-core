use hal::pass::Subpass;
use hal::pso::{
    read_spirv, AttributeDesc, BlendState, ColorBlendDesc, ColorMask, DescriptorSetLayoutBinding,
    Element, EntryPoint, GraphicsPipelineDesc, GraphicsShaderSet, Primitive, Rasterizer,
    ShaderStageFlags, Specialization, VertexBufferDesc, VertexInputRate,
};

use hal::format::Format;
use hal::{device::Device, Backend};

use std::io::Cursor;
use std::mem::size_of;
use std::sync::{Arc, Mutex};

use super::DeviceState;
use crate::rendering::shader::{ShaderData, ShaderType};

/// Manages the state of a pipeline
#[derive(Debug)]
pub struct PipelineState<B: Backend> {
    /// The backend-specific graphics pipeline object
    pub pipeline: Option<B::GraphicsPipeline>,
    /// The backend-specific pipeline layout
    pub pipeline_layout: Option<B::PipelineLayout>,
    /// The device which this pipeline is made for
    pub device: Arc<Mutex<DeviceState<B>>>,
    /// The layout vertices passed to this pipeline will have
    pub vertex_layout: Vec<DescriptorSetLayoutBinding>,
    /// TODO
    pub fragment_layout: Vec<DescriptorSetLayoutBinding>,
}

impl<B: Backend> PipelineState<B> {
    /// Creates a new `PipelineState`
    ///
    /// # Safety
    /// The layouts passed must be valid, as well as the render pass.
    pub unsafe fn new<IS>(
        descriptor_layouts: IS,
        render_pass: &B::RenderPass,
        device_state: &Arc<Mutex<DeviceState<B>>>,
        vertex_shader: ShaderData,
        fragment_shader: ShaderData,
    ) -> Self
    where
        IS: IntoIterator,
        IS::Item: std::borrow::Borrow<B::DescriptorSetLayout>,
    {
        // Get the backend device from the state
        let device = &device_state.lock().unwrap().device;

        let pipeline_layout = device
            .create_pipeline_layout(descriptor_layouts, &[(ShaderStageFlags::VERTEX, 0..8)])
            .expect("Failed to create pipeline layout");

        // Validate the passed ShaderDatas
        match vertex_shader.shader_type {
            ShaderType::VERTEX => {}
            ShaderType::FRAGMENT => panic!("Invalid vertex shader passed"),
        }
        match fragment_shader.shader_type {
            ShaderType::FRAGMENT => {}
            ShaderType::VERTEX => panic!("Invalid fragment shader passed"),
        }

        let pipeline = {
            // Take the two shaders and create shader modules with them
            let vs_module = {
                let spirv: Vec<u32> =
                    read_spirv(Cursor::new(&vertex_shader.shader_compiled[..])).unwrap();
                device.create_shader_module(&spirv).unwrap()
            };

            let fs_module = {
                let spirv: Vec<u32> =
                    read_spirv(Cursor::new(&fragment_shader.shader_compiled[..])).unwrap();
                device.create_shader_module(&spirv).unwrap()
            };

            let pipeline = {
                let (vs_entry, fs_entry) = (
                    EntryPoint::<B> {
                        entry: crate::rendering::ENTRY_POINT,
                        module: &vs_module,
                        specialization: hal::spec_const_list![0.8f32],
                    },
                    EntryPoint::<B> {
                        entry: crate::rendering::ENTRY_POINT,
                        module: &fs_module,
                        specialization: Specialization::default(),
                    },
                );

                let shader_entries = GraphicsShaderSet {
                    vertex: vs_entry,
                    hull: None,
                    domain: None,
                    geometry: None,
                    fragment: Some(fs_entry),
                };

                let subpass = Subpass {
                    index: 0,
                    main_pass: render_pass,
                };

                let mut pipeline_descriptor = GraphicsPipelineDesc::new(
                    shader_entries,
                    Primitive::TriangleList,
                    Rasterizer::FILL,
                    &pipeline_layout,
                    subpass,
                );
                pipeline_descriptor.blender.targets.push(ColorBlendDesc {
                    mask: ColorMask::ALL,
                    blend: Some(BlendState::ALPHA),
                });
                pipeline_descriptor.vertex_buffers.push(VertexBufferDesc {
                    binding: 0,
                    stride: size_of::<crate::rendering::Vertex>() as u32,
                    rate: VertexInputRate::Vertex,
                });

                pipeline_descriptor.attributes.push(AttributeDesc {
                    location: 0,
                    binding: 0,
                    element: Element {
                        format: Format::Rg32Sfloat,
                        offset: 0,
                    },
                });
                pipeline_descriptor.attributes.push(AttributeDesc {
                    location: 1,
                    binding: 0,
                    element: Element {
                        format: Format::Rg32Sfloat,
                        offset: 8,
                    },
                });

                device.create_graphics_pipeline(&pipeline_descriptor, None)
            };

            device.destroy_shader_module(vs_module);
            device.destroy_shader_module(fs_module);

            pipeline.unwrap()
        };

        Self {
            pipeline: Some(pipeline),
            pipeline_layout: Some(pipeline_layout),
            device: Arc::clone(&device_state),
            vertex_layout: vertex_shader.to_hal_layout(),
            fragment_layout: fragment_shader.to_hal_layout(),
        }
    }
}

impl<B: Backend> Drop for PipelineState<B> {
    fn drop(&mut self) {
        let device = &self.device.lock().unwrap().device;
        unsafe {
            device.destroy_graphics_pipeline(self.pipeline.take().unwrap());
            device.destroy_pipeline_layout(self.pipeline_layout.take().unwrap());
        }
    }
}
