use hal::{Backend, Instance};

use winit::window::Window;

use std::mem::ManuallyDrop;
use std::ptr;

use super::AdapterState;

/// Handles the state of the backend
pub struct BackendState<B: Backend> {
    /// The backend-specific instance
    pub instance: Option<B::Instance>,
    /// The surface that is being rendered to
    pub surface: ManuallyDrop<B::Surface>,
    /// The adapter being used for rendering
    pub adapter: AdapterState<B>,
    /// The `Window` object attached to this backend
    #[allow(dead_code)]
    pub window: Window,
}

impl<B: Backend> std::fmt::Debug for BackendState<B> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.debug_struct("BackendState").finish()
    }
}

impl<B: Backend> Drop for BackendState<B> {
    fn drop(&mut self) {
        if let Some(instance) = &self.instance {
            unsafe {
                let surface = ManuallyDrop::into_inner(ptr::read(&self.surface));
                instance.destroy_surface(surface);
            }
        }
    }
}
