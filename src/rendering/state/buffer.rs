use hal::adapter::MemoryType;
use hal::buffer::Usage;
use hal::device::Device;
use hal::memory::{Properties, Segment};
use hal::Backend;

use std::mem::size_of;
use std::ptr;
use std::sync::{Arc, Mutex};

use super::{AdapterState, DeviceState};
use crate::rendering::Dimensions;

/// Stores state related to a buffer on the GPU
#[derive(Debug)]
pub struct BufferState<B: Backend> {
    memory: Option<B::Memory>,
    buffer: Option<B::Buffer>,
    device: Arc<Mutex<DeviceState<B>>>,
    size: u64,
}

impl<B: Backend> BufferState<B> {
    /// Get the backend-specific buffer in use by the `BufferState`
    pub fn get_buffer(&self) -> &B::Buffer {
        self.buffer.as_ref().unwrap()
    }

    /// Creates a new `BufferState`
    ///
    /// # Safety
    /// Properties must be correct.
    pub unsafe fn new<T>(
        device: &Arc<Mutex<DeviceState<B>>>,
        data_source: &[T],
        usage: Usage,
        memory_types: &[MemoryType],
    ) -> Self
    where
        T: Copy,
    {
        // Define variables so the block below can write out to them
        let memory: B::Memory;
        let mut buffer: B::Buffer;
        let size: u64;

        // Get information about the data that is going to be uploaded
        let stride = size_of::<T>();
        let upload_size = data_source.len() * stride;

        {
            let device = &device.lock().unwrap().device;

            // Create the buffer and get information that is needed to put it on the GPU
            buffer = device.create_buffer(upload_size as u64, usage).unwrap();
            let memory_requirements = device.get_buffer_requirements(&buffer);

            let upload_type = memory_types
                .iter()
                .enumerate()
                .position(|(id, memory_type)| {
                    memory_requirements.type_mask & (1 << id) != 0
                        && memory_type
                            .properties
                            .contains(Properties::CPU_VISIBLE | Properties::COHERENT)
                })
                .unwrap()
                .into();

            // Allocate memory on the device
            memory = device
                .allocate_memory(upload_type, memory_requirements.size)
                .unwrap();

            // Map the memory so we can write into it
            device.bind_buffer_memory(&memory, 0, &mut buffer).unwrap();
            size = memory_requirements.size;

            let mapping = device.map_memory(&memory, Segment::ALL).unwrap();
            ptr::copy_nonoverlapping(data_source.as_ptr() as *const u8, mapping, upload_size);
            device.unmap_memory(&memory);
        }

        Self {
            memory: Some(memory),
            buffer: Some(buffer),
            device: Arc::clone(&device),
            size,
        }
    }

    /// Write new data into the buffer at offset `offset`
    pub fn update_data<T>(&mut self, offset: u64, data_source: &[T])
    where
        T: Copy,
    {
        let device = &self.device.lock().unwrap().device;

        // Calculate the size and stride of the data which is being uploaded
        let stride = size_of::<T>();
        let upload_size = data_source.len() * stride;

        let memory = self.memory.as_ref().unwrap();

        // Map the memory and upload the new data to the device
        unsafe {
            let mapping = device
                .map_memory(memory, Segment { offset, size: None })
                .unwrap();

            ptr::copy_nonoverlapping(data_source.as_ptr() as *const u8, mapping, upload_size);
            device.unmap_memory(memory);
        }
    }

    /// Create a `BufferState` initialized for an Image
    ///
    /// # Safety
    /// TODO
    pub unsafe fn for_image(
        device_state: &Arc<Mutex<DeviceState<B>>>,
        device: &B::Device,
        image: &::image::ImageBuffer<::image::Rgba<u8>, Vec<u8>>,
        adapter: &AdapterState<B>,
        usage: Usage,
    ) -> (Self, Dimensions<u32>, u32, usize) {
        let (width, height) = image.dimensions();

        let row_alignment_mask = adapter.limits.optimal_buffer_copy_pitch_alignment as u32 - 1;
        let stride = 4usize;

        let row_pitch = (width * stride as u32 + row_alignment_mask) & !row_alignment_mask;
        let upload_size = (height * row_pitch) as u64;

        let memory: B::Memory;
        let mut buffer: B::Buffer;
        let size: u64;

        {
            buffer = device.create_buffer(upload_size, usage).unwrap();
            let memory_requirements = device.get_buffer_requirements(&buffer);

            let upload_type = adapter
                .memory_types
                .iter()
                .enumerate()
                .position(|(id, memory_types)| {
                    memory_requirements.type_mask & (1 << id) != 0
                        && memory_types.properties.contains(
                            hal::memory::Properties::CPU_VISIBLE
                                | hal::memory::Properties::COHERENT,
                        )
                })
                .unwrap()
                .into();

            // Set up results for this code
            memory = device
                .allocate_memory(upload_type, memory_requirements.size)
                .unwrap();
            device.bind_buffer_memory(&memory, 0, &mut buffer).unwrap();
            size = memory_requirements.size;

            // Copy image into buffer
            let mapping = device
                .map_memory(&memory, hal::memory::Segment::ALL)
                .unwrap();
            for y in 0..height as usize {
                let data_source_slice =
                    &(**image)[y * (width as usize) * stride..(y + 1) * (width as usize) * stride];
                ptr::copy_nonoverlapping(
                    data_source_slice.as_ptr(),
                    mapping.offset(y as isize * row_pitch as isize),
                    data_source_slice.len(),
                );
            }
            device.unmap_memory(&memory);
        }

        (
            BufferState {
                memory: Some(memory),
                buffer: Some(buffer),
                device: Arc::clone(device_state),
                size,
            },
            Dimensions { width, height },
            row_pitch,
            stride,
        )
    }
}
