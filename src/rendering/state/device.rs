use hal::adapter::{Adapter, PhysicalDevice};
use hal::queue::{QueueFamily, QueueGroup};
use hal::window::Surface;
use hal::Backend;
use hal::Features;

/// Stores and handles the state of a device
#[derive(Debug)]
pub struct DeviceState<B: Backend> {
    /// Backend-specific device object
    pub device: B::Device,
    /// The physical device (ie: GPU) used by the logical device
    pub physical_device: B::PhysicalDevice,
    /// The queues available on the device
    pub queues: QueueGroup<B>,
}

impl<B: Backend> DeviceState<B> {
    /// Create a new DeviceState with the given adapter and surface
    pub fn new(adapter: Adapter<B>, surface: &B::Surface) -> Self {
        let family = adapter
            .queue_families
            .iter()
            .find(|family| {
                surface.supports_queue_family(family) && family.queue_type().supports_graphics()
            })
            .unwrap();

        let mut physical_device = unsafe {
            adapter
                .physical_device
                .open(&[(family, &[1.0])], Features::empty())
                .unwrap()
        };

        Self {
            device: physical_device.device,
            queues: physical_device.queue_groups.pop().unwrap(),
            physical_device: adapter.physical_device,
        }
    }
}
