use hal::buffer::{SubRange, Usage};
use hal::command::{
    ClearColor, ClearValue, CommandBuffer, CommandBufferFlags, Level, SubpassContents,
};
use hal::pool::{CommandPool, CommandPoolCreateFlags};
use hal::pso::{
    BufferDescriptorFormat, BufferDescriptorType, DescriptorPoolCreateFlags, DescriptorRangeDesc,
    DescriptorSetLayoutBinding, DescriptorType, ImageDescriptorType, PipelineStage, Rect,
    ShaderStageFlags, Viewport,
};
use hal::queue::{CommandQueue, Submission};
use hal::window::{SwapImageIndex, Swapchain};
use hal::{device::Device, Backend};

use std::io::Cursor;
use std::iter;
use std::sync::{Arc, Mutex, RwLock};

use super::{
    BackendState, DeviceState, FramebufferState, ImageState, PipelineState, RenderPassState,
    SwapchainState,
};
use crate::rendering::descriptor::DescSetLayout;
use crate::rendering::shader::ShaderData;
use crate::rendering::uniform::Uniform;
use crate::rendering::Renderable;

// This file is due for a major refactor, thus all the documentation is
// "TODO"

/// Handles rendering and stores the current state of the rendering pipeline
#[derive(Debug)]
pub struct RendererState<B: Backend> {
    /// The pool for creating descriptor sets
    pub uniform_descriptor_pool: Option<B::DescriptorPool>,
    image_descriptor_pool: Option<B::DescriptorPool>,
    swapchain: Option<SwapchainState<B>>,
    /// The DeviceState used by the renderer
    pub device: Arc<Mutex<DeviceState<B>>>,
    /// The BackendState used by the renderer
    pub backend: BackendState<B>,
    render_pass: RenderPassState<B>,
    uniform: Uniform<B>,
    framebuffer: FramebufferState<B>,
    viewport: Viewport,
    image: ImageState<B>,
    /// Whether the swapchain needs to be recreated
    pub recreate_swapchain: bool,
}

impl<B: Backend> RendererState<B> {
    /// Creates a new `RendererState`
    ///
    /// # Safety
    /// TODO
    pub unsafe fn new(mut backend: BackendState<B>) -> Self {
        let device = Arc::new(Mutex::new(DeviceState::new(
            backend.adapter.adapter.take().unwrap(),
            &backend.surface,
        )));

        let standard_image_descriptor = DescSetLayout::new(
            &device,
            &[
                DescriptorSetLayoutBinding {
                    binding: 0,
                    ty: DescriptorType::Image {
                        ty: ImageDescriptorType::Sampled {
                            with_sampler: false,
                        },
                    },
                    count: 1,
                    stage_flags: ShaderStageFlags::FRAGMENT,
                    immutable_samplers: false,
                },
                DescriptorSetLayoutBinding {
                    binding: 1,
                    ty: DescriptorType::Sampler,
                    count: 1,
                    stage_flags: ShaderStageFlags::FRAGMENT,
                    immutable_samplers: false,
                },
            ],
        );

        let uniform_descriptor = DescSetLayout::new(
            &device,
            &[DescriptorSetLayoutBinding {
                binding: 0,
                ty: DescriptorType::Buffer {
                    ty: BufferDescriptorType::Uniform,
                    format: BufferDescriptorFormat::Structured {
                        dynamic_offset: false,
                    },
                },
                count: 1,
                stage_flags: ShaderStageFlags::FRAGMENT,
                immutable_samplers: false,
            }],
        );

        let mut image_descriptor_pool = device
            .lock()
            .unwrap()
            .device
            .create_descriptor_pool(
                10, // Number of sets
                &[
                    DescriptorRangeDesc {
                        ty: DescriptorType::Image {
                            ty: ImageDescriptorType::Sampled {
                                with_sampler: false,
                            },
                        },
                        count: 1,
                    },
                    DescriptorRangeDesc {
                        ty: DescriptorType::Sampler,
                        count: 1,
                    },
                ],
                DescriptorPoolCreateFlags::empty(),
            )
            .ok();

        let mut uniform_descriptor_pool = device
            .lock()
            .unwrap()
            .device
            .create_descriptor_pool(
                10, // Number of sets
                &[DescriptorRangeDesc {
                    ty: DescriptorType::Buffer {
                        ty: BufferDescriptorType::Uniform,
                        format: BufferDescriptorFormat::Structured {
                            dynamic_offset: false,
                        },
                    },
                    count: 1,
                }],
                DescriptorPoolCreateFlags::empty(),
            )
            .ok();

        let image_descriptor =
            standard_image_descriptor.create_desc_set(image_descriptor_pool.as_mut().unwrap());
        let uniform_descriptor =
            uniform_descriptor.create_desc_set(uniform_descriptor_pool.as_mut().unwrap());

        const IMAGE_LOGO: &[u8] = include_bytes!("../../../data/logo.png");
        let image = image::load(Cursor::new(&IMAGE_LOGO[..]), image::PNG)
            .unwrap()
            .to_rgba();

        let mut staging_pool = {
            let device = device.lock().unwrap();
            device
                .device
                .create_command_pool(device.queues.family, CommandPoolCreateFlags::empty())
                .expect("Failed to create staging command pool")
        };

        let image = ImageState::new(
            image_descriptor,
            &image,
            &backend.adapter,
            Usage::TRANSFER_SRC,
            &mut device.lock().unwrap(),
            &mut staging_pool,
        );

        let uniform = Uniform::new(
            Arc::clone(&device),
            &backend.adapter.memory_types,
            &[1f32, 1.0f32, 1.0f32, 1.0f32],
            uniform_descriptor,
            0,
        );

        image.wait_for_transfer_completion();

        device
            .lock()
            .unwrap()
            .device
            .destroy_command_pool(staging_pool);

        let mut swapchain = Some(SwapchainState::new(&mut backend, &device));
        let render_pass = RenderPassState::new(swapchain.as_ref().unwrap(), &device);
        let framebuffer = FramebufferState::new(&device, &render_pass, swapchain.as_mut().unwrap());
        let viewport = RendererState::create_viewport(swapchain.as_ref().unwrap());
        RendererState {
            backend,
            device,
            image,
            image_descriptor_pool,
            uniform_descriptor_pool,
            uniform,
            render_pass,
            swapchain,
            framebuffer,
            viewport,
            recreate_swapchain: false,
        }
    }

    /// TODO
    pub fn make_pipeline(
        &mut self,
        vertex_shader: ShaderData,
        fragment_shader: ShaderData,
        image_layout: &B::DescriptorSetLayout,
        uniform_layout: &B::DescriptorSetLayout,
    ) -> PipelineState<B> {
        unsafe {
            PipelineState::new(
                vec![image_layout, uniform_layout],
                self.render_pass.render_pass.as_ref().unwrap(),
                &self.device,
                vertex_shader,
                fragment_shader,
            )
        }
    }

    /// TODO
    pub fn load_image(&mut self, _path: &'static str) -> ImageState<B> {
        // Create the descriptor for the image
        let image_descriptor = unsafe {
            DescSetLayout::new(
                &self.device,
                &[
                    DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::Image {
                            ty: ImageDescriptorType::Sampled {
                                with_sampler: false,
                            },
                        },
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false,
                    },
                    DescriptorSetLayoutBinding {
                        binding: 1,
                        ty: DescriptorType::Sampler,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false,
                    },
                ],
            )
        };

        let image_descriptor = unsafe {
            image_descriptor.create_desc_set(self.image_descriptor_pool.as_mut().unwrap())
        };

        // TEMP
        const IMAGE_LOGO: &[u8] = include_bytes!("../../../data/logo.png");
        let image = image::load(Cursor::new(&IMAGE_LOGO[..]), image::PNG)
            .unwrap()
            .to_rgba();

        let mut locked_device = self.device.lock().unwrap();
        // Create a command pool that will be used to upload the image
        let mut staging_pool = unsafe {
            locked_device
                .device
                .create_command_pool(locked_device.queues.family, CommandPoolCreateFlags::empty())
                .expect("Failed to create staging command pool")
        };

        // Return the ImageState
        let image = unsafe {
            ImageState::new(
                image_descriptor,
                &image,
                &self.backend.adapter,
                Usage::TRANSFER_SRC,
                &mut locked_device,
                &mut staging_pool,
            )
        };

        // Wait for the image to be uploaded and destroy the command pool
        drop(locked_device);
        image.wait_for_transfer_completion();

        let locked_device = self.device.lock().unwrap();
        unsafe { locked_device.device.destroy_command_pool(staging_pool) };

        image
    }

    /// TODO
    pub fn recreate_swapchain(&mut self) {
        self.device.lock().unwrap().device.wait_idle().unwrap();

        self.swapchain.take().unwrap();

        self.swapchain = Some(unsafe { SwapchainState::new(&mut self.backend, &self.device) });

        self.render_pass =
            unsafe { RenderPassState::new(self.swapchain.as_ref().unwrap(), &self.device) };

        self.framebuffer = unsafe {
            FramebufferState::new(
                &self.device,
                &self.render_pass,
                self.swapchain.as_mut().unwrap(),
            )
        };

        // TODO: Update pipelines

        self.viewport = RendererState::create_viewport(self.swapchain.as_ref().unwrap());
    }

    /// TODO
    pub fn create_viewport(swapchain: &SwapchainState<B>) -> Viewport {
        Viewport {
            rect: Rect {
                x: 0,
                y: 0,
                w: swapchain.extent.width as i16,
                h: swapchain.extent.height as i16,
            },
            depth: 0.0..1.0,
        }
    }

    /// Renders all objects in the vec
    pub fn render(&mut self, renderables: Vec<Renderable<B>>) {
        if self.recreate_swapchain {
            self.recreate_swapchain();
            self.recreate_swapchain = false;
        }

        let sem_index = self.framebuffer.next_acq_pre_pair_index();

        let frame: SwapImageIndex = unsafe {
            let (acquire_semaphore, _) = self
                .framebuffer
                .get_frame_data(None, Some(sem_index))
                .1
                .unwrap();
            match self
                .swapchain
                .as_mut()
                .unwrap()
                .swapchain
                .as_mut()
                .unwrap()
                .acquire_image(!0, Some(acquire_semaphore), None)
            {
                Ok((i, _)) => i,
                Err(_) => {
                    self.recreate_swapchain = true;
                    return;
                }
            }
        };

        let (fid, sid) = self
            .framebuffer
            .get_frame_data(Some(frame as usize), Some(sem_index));
        let (framebuffer_fence, framebuffer, command_pool, command_buffers) = fid.unwrap();
        let (image_acquired, image_present) = sid.unwrap();
        unsafe {
            // Wait for the framebuffer to be ready
            self.device
                .lock()
                .unwrap()
                .device
                .wait_for_fence(framebuffer_fence, !0)
                .unwrap();

            // Reset the fence
            self.device
                .lock()
                .unwrap()
                .device
                .reset_fence(framebuffer_fence)
                .unwrap();

            // Clear the command pool and buffers
            command_pool.reset(false);

            // Rendering

            // Create a new command buffer or grab one
            let mut command_buffer = match command_buffers.pop() {
                Some(command_buffer) => command_buffer,
                None => command_pool.allocate_one(Level::Primary),
            };

            command_buffer.begin_primary(CommandBufferFlags::ONE_TIME_SUBMIT);

            // Set the viewport settings
            command_buffer.set_viewports(0, &[self.viewport.clone()]);
            command_buffer.set_scissors(0, &[self.viewport.rect]);

            command_buffer.begin_render_pass(
                self.render_pass.render_pass.as_ref().unwrap(),
                framebuffer,
                self.viewport.rect,
                &[ClearValue {
                    color: ClearColor {
                        float32: [0.0, 1.0, 1.0, 1.0],
                    },
                }],
                SubpassContents::Inline,
            );
            for renderable in &renderables {
                command_buffer.bind_graphics_pipeline(
                    renderable
                        .pipeline
                        .read()
                        .unwrap()
                        .pipeline
                        .as_ref()
                        .unwrap(),
                );
                command_buffer.bind_vertex_buffers(
                    0,
                    Some((
                        renderable.mesh.read().unwrap().mesh_data.get_buffer(),
                        SubRange::WHOLE,
                    )),
                );
                command_buffer.bind_graphics_descriptor_sets(
                    renderable
                        .pipeline
                        .read()
                        .unwrap()
                        .pipeline_layout
                        .as_ref()
                        .unwrap(),
                    0,
                    vec![
                        // Submit the image descriptor first
                        renderable
                            .texture
                            .read()
                            .unwrap()
                            .descriptor
                            .set
                            .as_ref()
                            .unwrap(),
                        // Then submit the uniform descriptor
                        // TODO: Support more than one descriptor in the future
                        self.uniform
                            .descriptor
                            .as_ref()
                            .unwrap()
                            .set
                            .as_ref()
                            .unwrap(),
                    ],
                    &[],
                );
                command_buffer.draw(0..6, 0..1);
            }
            command_buffer.end_render_pass();
            command_buffer.finish();

            // Generate the submission to send to the GPU
            let submission = Submission {
                command_buffers: iter::once(&command_buffer),
                wait_semaphores: iter::once((&*image_acquired, PipelineStage::BOTTOM_OF_PIPE)),
                signal_semaphores: iter::once(&*image_present),
            };

            // Submit the buffer
            self.device.lock().unwrap().queues.queues[0]
                .submit(submission, Some(framebuffer_fence));
            command_buffers.push(command_buffer);

            // Present the frame
            if let Err(_) = self
                .swapchain
                .as_ref()
                .unwrap()
                .swapchain
                .as_ref()
                .unwrap()
                .present(
                    &mut self.device.lock().unwrap().queues.queues[0],
                    frame,
                    Some(&*image_present),
                )
            {
                self.recreate_swapchain = true;
                return;
            }
        }
    }
}

impl<B: Backend> Drop for RendererState<B> {
    fn drop(&mut self) {
        self.device.lock().unwrap().device.wait_idle().unwrap();
        unsafe {
            self.device
                .lock()
                .unwrap()
                .device
                .destroy_descriptor_pool(self.image_descriptor_pool.take().unwrap());
            self.device
                .lock()
                .unwrap()
                .device
                .destroy_descriptor_pool(self.uniform_descriptor_pool.take().unwrap());
            self.swapchain.take();
        }
    }
}
