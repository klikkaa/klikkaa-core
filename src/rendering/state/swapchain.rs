use hal::format::{ChannelType, Format};
use hal::image::Extent;
use hal::window::{PresentMode, Surface, SwapchainConfig};
use hal::{device::Device, Backend};

use std::sync::{Arc, Mutex};

use super::{BackendState, DeviceState};

/// Stores the state of a Swapchain
#[derive(Debug)]
pub struct SwapchainState<B: Backend> {
    /// The backend-specific swapchain
    pub swapchain: Option<B::Swapchain>,
    /// The backbuffer for the swapchain
    pub backbuffer: Option<Vec<B::Image>>,
    /// The DeviceState that this swapchain exists on
    pub device: Arc<Mutex<DeviceState<B>>>,
    /// The size of the swapchain
    pub extent: Extent,
    /// The format of the swapchain
    pub format: Format,
}

impl<B: Backend> SwapchainState<B> {
    /// Creates a new `SwapchainState`
    ///
    /// # Safety
    /// TODO
    pub unsafe fn new(backend: &mut BackendState<B>, device: &Arc<Mutex<DeviceState<B>>>) -> Self {
        // Get the capabilities required for the surface
        let capabilities = backend
            .surface
            .capabilities(&device.lock().unwrap().physical_device);

        // Get the surface's supported formats
        let formats = backend
            .surface
            .supported_formats(&device.lock().unwrap().physical_device);

        // TODO: Print debug info

        // Pick a format which is the best to use for the swapchain
        let format = formats.map_or(Format::Rgba8Srgb, |formats| {
            formats
                .iter()
                .find(|format| format.base_format().1 == ChannelType::Srgb)
                .copied()
                .unwrap_or(formats[0])
        });

        let mut swapchain_config =
            SwapchainConfig::from_caps(&capabilities, format, crate::rendering::DEFAULT_DIMENSIONS);

        // TODO: Fix this in the future where we render every update but present on VSYNC
        // With the current rendering implementation, this is not possible yet.
        swapchain_config.present_mode = PresentMode::IMMEDIATE;

        let extent = swapchain_config.extent.to_extent();

        let (swapchain, backbuffer) = device
            .lock()
            .unwrap()
            .device
            .create_swapchain(&mut backend.surface, swapchain_config, None)
            .expect("Failed to create swapchain");

        Self {
            swapchain: Some(swapchain),
            backbuffer: Some(backbuffer),
            device: Arc::clone(device),
            extent,
            format,
        }
    }
}

impl<B: Backend> Drop for SwapchainState<B> {
    fn drop(&mut self) {
        unsafe {
            self.device
                .lock()
                .unwrap()
                .device
                .destroy_swapchain(self.swapchain.take().unwrap());
        }
    }
}
