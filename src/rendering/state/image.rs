use hal::buffer::Usage as BufferUsage;
use hal::command::{BufferImageCopy, CommandBuffer, CommandBufferFlags, Level};
use hal::format::{AsFormat, Aspects, Swizzle};
use hal::image::Kind::D2;
use hal::image::{
    Access, Extent, Filter, Layout, Offset, SamplerDesc, Size, SubresourceLayers, Tiling, Usage,
    ViewCapabilities, ViewKind, WrapMode,
};
use hal::memory::{Barrier, Dependencies, Properties};
use hal::pool::CommandPool;
use hal::pso::{Descriptor, PipelineStage};
use hal::queue::CommandQueue;
use hal::{device::Device, Backend};

use std::iter;

use super::{AdapterState, BufferState, DeviceState};
use crate::rendering::descriptor::{DescSet, DescSetWrite};

/// Handles the state of an Image which has been uploaded to the GPU
#[derive(Debug)]
pub struct ImageState<B: Backend> {
    /// The descriptor set of the image
    pub descriptor: DescSet<B>,
    buffer: Option<BufferState<B>>,
    sampler: Option<B::Sampler>,
    image_view: Option<B::ImageView>,
    image: Option<B::Image>,
    memory: Option<B::Memory>,
    transferred_image_fence: Option<B::Fence>,
}

impl<B: Backend> ImageState<B> {
    /// Creates a new `ImageState`
    ///
    /// # Safety
    /// TODO
    pub unsafe fn new(
        mut descriptor: DescSet<B>,
        // TODO: Understand this syntax so I can get rid of it
        image: &image::ImageBuffer<::image::Rgba<u8>, Vec<u8>>,
        adapter: &AdapterState<B>,
        usage: BufferUsage,
        device_state: &mut DeviceState<B>,
        staging_pool: &mut B::CommandPool,
    ) -> Self {
        // Create the buffer the image will be stored in
        let (buffer, dimensions, row_pitch, stride) = BufferState::for_image(
            &descriptor.layout.device,
            &device_state.device,
            image,
            adapter,
            usage,
        );

        let buffer = Some(buffer);
        let device = &mut device_state.device;

        // Create the image
        let kind = D2(dimensions.width as Size, dimensions.height as Size, 1, 1);
        let mut image = device
            .create_image(
                kind,
                1,
                crate::rendering::ColorFormat::SELF,
                Tiling::Optimal,
                Usage::TRANSFER_DST | Usage::SAMPLED,
                ViewCapabilities::empty(),
            )
            .unwrap();

        let requirements = device.get_image_requirements(&image);

        let device_type = adapter
            .memory_types
            .iter()
            .enumerate()
            .position(|(id, memory_type)| {
                requirements.type_mask & (1 << id) != 0
                    && memory_type.properties.contains(Properties::DEVICE_LOCAL)
            })
            .unwrap()
            .into();

        let memory = device
            .allocate_memory(device_type, requirements.size)
            .unwrap();

        device.bind_image_memory(&memory, 0, &mut image).unwrap();
        let image_view = device
            .create_image_view(
                &image,
                ViewKind::D2,
                crate::rendering::ColorFormat::SELF,
                Swizzle::NO,
                crate::rendering::COLOR_RANGE.clone(),
            )
            .unwrap();

        let sampler = device
            .create_sampler(&SamplerDesc::new(Filter::Linear, WrapMode::Clamp))
            .expect("Failed to create sampler");

        descriptor.write_to_state(
            vec![
                DescSetWrite {
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Image(
                        &image_view,
                        Layout::ShaderReadOnlyOptimal,
                    )),
                },
                DescSetWrite {
                    binding: 1,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Sampler(&sampler)),
                },
            ],
            device,
        );

        let transferred_image_fence = device.create_fence(false).expect("Failed to create fence");

        {
            let mut command_buffer = staging_pool.allocate_one(Level::Primary);
            command_buffer.begin_primary(CommandBufferFlags::ONE_TIME_SUBMIT);

            let image_barrier = Barrier::Image {
                states: (Access::empty(), Layout::Undefined)
                    ..(Access::TRANSFER_WRITE, Layout::TransferDstOptimal),
                target: &image,
                families: None,
                range: crate::rendering::COLOR_RANGE.clone(),
            };

            command_buffer.pipeline_barrier(
                PipelineStage::TOP_OF_PIPE..PipelineStage::TRANSFER,
                Dependencies::empty(),
                &[image_barrier],
            );

            command_buffer.copy_buffer_to_image(
                buffer.as_ref().unwrap().get_buffer(),
                &image,
                Layout::TransferDstOptimal,
                &[BufferImageCopy {
                    buffer_offset: 0,
                    buffer_width: row_pitch / (stride as u32),
                    buffer_height: dimensions.height as u32,
                    image_layers: SubresourceLayers {
                        aspects: Aspects::COLOR,
                        level: 0,
                        layers: 0..1,
                    },
                    image_offset: Offset { x: 0, y: 0, z: 0 },
                    image_extent: Extent {
                        width: dimensions.width,
                        height: dimensions.height,
                        depth: 1,
                    },
                }],
            );

            let image_barrier = Barrier::Image {
                states: (Access::TRANSFER_WRITE, Layout::TransferDstOptimal)
                    ..(Access::SHADER_READ, Layout::ShaderReadOnlyOptimal),
                target: &image,
                families: None,
                range: crate::rendering::COLOR_RANGE.clone(),
            };

            command_buffer.pipeline_barrier(
                PipelineStage::TRANSFER..PipelineStage::FRAGMENT_SHADER,
                Dependencies::empty(),
                &[image_barrier],
            );

            command_buffer.finish();

            device_state.queues.queues[0].submit_without_semaphores(
                iter::once(&command_buffer),
                Some(&transferred_image_fence),
            );
        };

        Self {
            descriptor,
            buffer,
            sampler: Some(sampler),
            image_view: Some(image_view),
            image: Some(image),
            memory: Some(memory),
            transferred_image_fence: Some(transferred_image_fence),
        }
    }

    /// Waits for an ImageState to be transferred to the gpu
    ///
    /// This **WILL** block the thread its called on until the fence is set
    pub fn wait_for_transfer_completion(&self) {
        let device = &self.descriptor.layout.device.lock().unwrap().device;
        unsafe {
            device
                .wait_for_fence(self.transferred_image_fence.as_ref().unwrap(), !0)
                .unwrap();
        }
    }

    /// Returns the descriptor layout for the image
    pub fn get_layout(&self) -> &B::DescriptorSetLayout {
        self.descriptor.get_layout()
    }
}

impl<B: Backend> Drop for ImageState<B> {
    fn drop(&mut self) {
        unsafe {
            let device = &self.descriptor.layout.device.lock().unwrap().device;

            let fence = self.transferred_image_fence.take().unwrap();
            device.wait_for_fence(&fence, !0).unwrap();
            device.destroy_fence(fence);

            device.destroy_sampler(self.sampler.take().unwrap());
            device.destroy_image_view(self.image_view.take().unwrap());
            device.destroy_image(self.image.take().unwrap());
            device.free_memory(self.memory.take().unwrap());
        }

        self.buffer.take().unwrap();
    }
}
