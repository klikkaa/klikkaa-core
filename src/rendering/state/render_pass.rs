use hal::image::Layout;
use hal::pass::{Attachment, AttachmentLoadOp, AttachmentOps, AttachmentStoreOp, SubpassDesc};
use hal::{device::Device, Backend};

use std::sync::{Arc, Mutex};

use super::{DeviceState, SwapchainState};

/// Handles the state of a render pass
#[derive(Debug)]
pub struct RenderPassState<B: Backend> {
    /// The backend-specific render pass type
    pub render_pass: Option<B::RenderPass>,
    /// The device which this renderpass is used for
    pub device: Arc<Mutex<DeviceState<B>>>,
}

impl<B: Backend> RenderPassState<B> {
    /// Creates a new `RenderPassState`
    ///
    /// # Safety
    /// TODO
    /// Pretty sure this function is safe as long as you followed the safety instructions for the arguments
    pub unsafe fn new(swapchain: &SwapchainState<B>, device: &Arc<Mutex<DeviceState<B>>>) -> Self {
        let render_pass = {
            let attachment = Attachment {
                format: Some(swapchain.format),
                samples: 1,
                ops: AttachmentOps::new(AttachmentLoadOp::Clear, AttachmentStoreOp::Store),
                stencil_ops: AttachmentOps::DONT_CARE,
                layouts: Layout::Undefined..Layout::Present,
            };

            let subpass = SubpassDesc {
                colors: &[(0, Layout::ColorAttachmentOptimal)],
                depth_stencil: None,
                inputs: &[],
                resolves: &[],
                preserves: &[],
            };

            device
                .lock()
                .unwrap()
                .device
                .create_render_pass(&[attachment], &[subpass], &[])
                .ok()
        };

        Self {
            render_pass,
            device: Arc::clone(&device),
        }
    }
}

impl<B: Backend> Drop for RenderPassState<B> {
    fn drop(&mut self) {
        let device = &self.device.lock().unwrap().device;
        unsafe {
            device.destroy_render_pass(self.render_pass.take().unwrap());
        }
    }
}
