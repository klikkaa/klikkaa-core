use hal::adapter::{Adapter, MemoryType, PhysicalDevice};
use hal::{Backend, Limits};

/// Handles the state of an adapter
#[derive(Debug)]
pub struct AdapterState<B: Backend> {
    /// The backend-specific adapter
    pub adapter: Option<Adapter<B>>,
    /// The memory types which are available
    pub memory_types: Vec<MemoryType>,
    /// The limits on this adapter
    pub limits: Limits,
}

impl<B: Backend> AdapterState<B> {
    /// Creates a new `AdapterState` from the given backend specific adapters
    ///
    /// TODO: Choose the most appropriate adapter, right now it just uses the first one
    pub fn new(adapters: &mut Vec<Adapter<B>>) -> Self {
        Self::new_adapter(adapters.remove(0))
    }

    /// Creates a new AdapterState from a single backend-specifc adapter
    fn new_adapter(adapter: Adapter<B>) -> Self {
        let memory_types = adapter.physical_device.memory_properties().memory_types;
        let limits = adapter.physical_device.limits();

        AdapterState {
            adapter: Some(adapter),
            memory_types,
            limits,
        }
    }
}
