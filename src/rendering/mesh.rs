use hal::Backend;

use crate::rendering::state::BufferState;

/// A mesh
#[derive(Debug)]
pub struct Mesh<B: Backend> {
    /// The raw mesh data
    pub mesh_data: BufferState<B>,
}
