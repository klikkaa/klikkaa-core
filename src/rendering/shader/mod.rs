//! Wraps shaders
//!
//! Stores descriptor layouts, handles compiling and saving the shader to disk,
//! and loading `ShaderData` from disk.

mod load;
mod save;

use gfx_hal::pso::{
    BufferDescriptorFormat, BufferDescriptorType, DescriptorSetLayoutBinding, DescriptorType,
    ShaderStageFlags,
};
use glsl_to_spirv::ShaderType as ExternalShaderType;
use serde::{Deserialize, Serialize};

use std::io::Read;

/// Stores shaders and their associated layouts
///
/// The shader is stored as compiled SPIRV
#[derive(Serialize, Deserialize, Debug)]
pub struct ShaderData {
    /// The type of shader being stored, also known as the stage
    pub shader_type: ShaderType,
    /// The compiled bytecode in spirv
    pub shader_compiled: Vec<u8>,
    /// The layouts that the shader uses for descriptors
    pub descriptor_layouts: Vec<ShaderLayout>,
}

/// A layout for a descriptor used in a ShaderData.
#[derive(Serialize, Deserialize, Debug)]
pub struct ShaderLayout {
    /// The binding of the descriptor
    ///
    /// Refer to vulkan docs if confused.
    pub binding: u32,
    /// The type of buffer being represented by the `ShaderLayout`
    pub ty: BufferType,
}

/// Represents types of buffers that can be represented by `ShaderLayout`
#[derive(Serialize, Deserialize, Debug)]
pub enum BufferType {
    /// Uniform Buffer
    Uniform,
}

/// An error that occurs when creating, loading, or saving a `ShaderData`
#[derive(Debug)]
pub enum ShaderDataError {
    /// An error from shader compilation
    ShaderCompilationError(String),
    /// An error from reading/writing to a file
    FilesystemError(std::io::Error),
    /// An error from deserialization of `ShaderData`
    DeserializationError,
    /// An error from serialization of `ShaderData`
    SerializationError,
}

/// The type, or stage a shader should be compiled for
#[derive(Serialize, Deserialize, Debug)]
pub enum ShaderType {
    /// Vertex Shader
    VERTEX,
    /// Frament Shader
    FRAGMENT,
}

impl ShaderData {
    /// Creates a new `ShaderData`
    ///
    /// `shader_code` is a string of glsl shader program code
    pub fn new(
        ty: ShaderType,
        shader_code: String,
        layouts: Vec<ShaderLayout>,
    ) -> Result<Self, ShaderDataError> {
        // Get the shader type for the glsl compiler
        let shader_stage = match ty {
            ShaderType::VERTEX => ExternalShaderType::Vertex,
            ShaderType::FRAGMENT => ExternalShaderType::Fragment,
        };

        // Compile the shader
        let mut compiled_shader = match glsl_to_spirv::compile(&shader_code, shader_stage) {
            Ok(spirv) => spirv,
            Err(error) => return Err(ShaderDataError::ShaderCompilationError(error)),
        };

        // Read the shader to a Vec of bytes
        let mut shader_buffer: Vec<u8> = vec![];
        match compiled_shader.read_to_end(&mut shader_buffer) {
            Ok(_) => {}
            Err(error) => return Err(ShaderDataError::FilesystemError(error)),
        };

        Ok(Self {
            shader_type: ty,
            shader_compiled: shader_buffer,
            descriptor_layouts: layouts,
        })
    }

    /// Returns the buffer layouts in a form that can be used with the hal
    pub fn to_hal_layout(&self) -> Vec<DescriptorSetLayoutBinding> {
        let mut layout_bindings = vec![];

        // Get the hal stage flag for the shader
        let stage_flags = match &self.shader_type {
            ShaderType::VERTEX => ShaderStageFlags::VERTEX,
            ShaderType::FRAGMENT => ShaderStageFlags::FRAGMENT,
        };

        // Generate the layout bindings
        for layout in &self.descriptor_layouts {
            // Get the hal buffer type from the internal layout format
            let buffer_type = match layout.ty {
                BufferType::Uniform => BufferDescriptorType::Uniform,
            };

            // Create the binding and add it to the vector
            let binding = DescriptorSetLayoutBinding {
                binding: layout.binding,
                ty: DescriptorType::Buffer {
                    ty: buffer_type,
                    format: BufferDescriptorFormat::Structured {
                        dynamic_offset: false,
                    },
                },
                count: 1,
                stage_flags,
                immutable_samplers: false,
            };

            layout_bindings.push(binding);
        }

        layout_bindings
    }
}
