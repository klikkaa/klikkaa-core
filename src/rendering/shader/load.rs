use std::fs::File;

use serde_bare::from_reader;

use super::{ShaderData, ShaderDataError};

impl ShaderData {
    /// Loads a shader from disk with name `name`, currently uses a hardcoded shader folder path
    pub fn load_from_disk<S: ToString>(name: S) -> Result<ShaderData, ShaderDataError> {
        // Try to open the file
        let file = File::open(format!("shaders/{}", name.to_string()));
        let file = match file {
            Ok(file) => file,
            Err(error) => return Err(ShaderDataError::FilesystemError(error)),
        };

        // Deserialize the file and return the data
        let deserialized_file: Result<ShaderData, _> = from_reader(&file);
        match deserialized_file {
            Ok(shader_data) => Ok(shader_data),
            Err(_) => Err(ShaderDataError::DeserializationError),
        }
    }
}
