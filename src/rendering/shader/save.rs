use serde_bare::to_writer;

use std::fs::File;

use super::{ShaderData, ShaderDataError};

impl ShaderData {
    /// Saves a ShaderData to an arbitrary folder with name `name`
    pub fn save<S: ToString>(&self, name: S) -> Result<(), ShaderDataError> {
        // Try to open the target file
        let file = {
            match File::create(format!("shaders/{}", name.to_string())) {
                Ok(file) => file,
                Err(error) => return Err(ShaderDataError::FilesystemError(error)),
            }
        };

        // Write the data to the disk
        match to_writer(file, self) {
            Ok(_) => {}
            Err(_) => return Err(ShaderDataError::SerializationError),
        };

        Ok(())
    }
}
