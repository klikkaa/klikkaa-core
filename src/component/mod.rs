//! ECS Component types

use glam::Quat;

use lua::Lua as LuaState;

use std::fmt::{Debug, Formatter, Result};
use std::sync::{Arc, Mutex, RwLock};

use crate::rendering::backend::Backend;
use crate::rendering::state::{ImageState, PipelineState};
use crate::rendering::Mesh;
use crate::rendering::Vector3;

/// Contains a Vector3, represents the position of an Entity
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct PositionComponent(pub Vector3);

/// Contains a `Quat`, represents the rotation of an Entity
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct RotationComponent(pub Quat);

/// Contains an `ImageState`, represents a texture of an Entity
#[derive(Clone, Debug)]
pub struct TextureComponent(pub Arc<RwLock<ImageState<Backend>>>);

/// Contains a `PipelineStates`, represents the pipeline used to render an Entity
#[derive(Clone, Debug)]
pub struct PipelineComponent(pub Arc<RwLock<PipelineState<Backend>>>);

/// Contains a `Mesh`, represents the mesh used to render an Entity
#[derive(Clone, Debug)]
pub struct MeshComponent(pub Arc<RwLock<Mesh<Backend>>>);

/// Contains a string, used as a "Name" for an Entity
#[derive(Clone, Debug)]
pub struct NameComponent(pub String);

/// Contains a lua state and associated lua code
///
/// # Safety
/// Setting up the lua state for execution on the world is an involved task, and therefore if a new execution point is required it is important to ensure that it is safe and workable.
#[derive(Clone)]
pub struct LuaComponent(pub Arc<Mutex<LuaState>>, pub String);

impl Debug for LuaComponent {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_struct("Luacomponent").finish()
    }
}

/// Contains data for projecting the scene
#[derive(Clone, Debug)]
pub struct CameraComponent {
    /// Field of View, in degrees
    pub fov: f32,
}
